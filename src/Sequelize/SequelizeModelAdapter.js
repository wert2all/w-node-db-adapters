'use strict';
const ModelInterface = require('w-node-db-model');
const ModelQueryAdapter = require('./SequelizeQueryAdapter');
const EMEntity = require('w-node-db-entity');

/**
 * @class SequelizeModelAdapter
 * @type {DBModelInterface}
 */
class SequelizeModelAdapter extends ModelInterface {
    /**
     *
     * @param model
     * @param {DBModelDefinitionInterface} modelDefinition
     */
    constructor(model, modelDefinition) {
        super();
        this._sequelizeModelClass = model;
        /**
         *
         * @private
         */
        this._sequelizeModel = null;
        /**
         *
         * @type {DBModelDefinitionInterface}
         * @private
         */
        this._modelDefinition = modelDefinition;
        /**
         *
         * @type {DBModelRelationInterface}
         * @private
         */
        this._relation = this._modelDefinition.getRelation();
    }

    /**
     *
     * @param {{}} data
     * @return {Promise<DBModelInterface>}
     */
    save(data) {
        Object
            .keys(data)
            .forEach(key => this._sequelizeModel.setDataValue(key, data[key]));
        return this._sequelizeModel
            .save()
            .then(() => this);
    }

    /**
     *
     * @param {{}} data
     * @return {DBModelInterface}
     */
    create(data) {
        return this._cloneObject({
            _sequelizeModel: new this._sequelizeModelClass(data)
        });
    }

    /**
     *
     * @param {{}} dataObject
     * @return {DBModelInterface}
     * @private
     */
    _cloneObject(dataObject) {
        let clone = Object.assign(Object.create(Object.getPrototypeOf(this)), this);
        clone = Object.assign(clone, dataObject);
        return clone;
    }

    /**
     *
     * @param {DBModelInterface} child
     * @return {DBModelInterface}
     */
    addChild(child) {
        if (this._relation) {
            this._sequelizeModel[this._getRelationMethodName()](
                child.getPrimaryKeyValue()
            );
        }

        return this;
    }

    getPrimaryKeyValue() {
        return this._sequelizeModel.get('id');
    }

    _getRelationMethodName() {
        function jsUcfirst(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        return 'add' + jsUcfirst(this._relation.getRelationAlias());
    }

    /**
     * @return {DBModelDefinitionInterface}
     */
    getModelDefinition() {
        return this._modelDefinition;
    }

    /**
     *
     * @return {DBModelQueryInterface}
     */
    getModelQuery() {
        return new ModelQueryAdapter();
    }

    /**
     *
     * @param {DBModelQueryInterface} query
     * @return {Promise<EMEntity>}
     */
    fetch(query) {
        return this._sequelizeModelClass
            .findAll(query.getQuery())
            .then(data =>
                data.map(item => {
                        const entity = this._createExistEntity(item);
                        const entityRelation = entity.getModel()
                            .getModelDefinition()
                            .getRelation();
                        if (entityRelation !== null) {
                            const childEntities = item.get(
                                entityRelation.getRelationAlias()
                            );
                            childEntities
                                .map(child => this._createExistEntity(child))
                                .forEach(childEntity => entity.addChild(childEntity));
                        }
                        return entity;
                    }
                )
            );
    }

    /**
     *
     * @param item
     * @return {EMEntity}
     * @private
     */
    _createExistEntity(item) {
        return new EMEntity(
            this._cloneObject({_sequelizeModel: item}),
            item.get()
        );
    }
}

module.exports = SequelizeModelAdapter;
