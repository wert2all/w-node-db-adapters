'use strict';
const DBModelQueryInterface = require('w-node-db-model-query');

/**
 * @class SequelizeQueryAdapter
 * @type DBModelQueryInterface
 */
class SequelizeQueryAdapter extends DBModelQueryInterface {

    constructor() {
        super();
        /**
         *
         * @type {Array}
         * @private
         */
        this._fieldsList = [];
        /**
         *
         * @type {[]}
         * @private
         */
        this._orders = [];
        /**
         *
         * @type {{offset: number, limit: number|null}}
         * @private
         */
        this._limit = {
            offset: 0,
            limit: null
        };

        /**
         *
         * @type {[]}
         * @private
         */
        this._filters = [];
        /**
         *
         * @type {DBModelRelationInterface[]}
         * @private
         */
        this._relations = [];
    }

    /**
     *
     * @return {{}}
     */
    getQuery() {
        const ret = this._createWhere({});
        this._relations.filter(relation => !!relation);
        if (this._relations.length > 0) {
            ret['include'] = this._relations
                .map(relation => relation.getRelationAlias());
        }
        if (this._fieldsList.length > 0) {
            ret['attributes'] = this._fieldsList;
        }
        if (this._orders.length > 0) {
            ret['order'] = this._orders;
        }
        if (this._limit.offset) {
            ret['offset'] = this._limit.offset;
        }
        if (this._limit.limit) {
            ret['limit'] = this._limit.limit;
        }

        return ret;
    }

    /**
     *
     * @param {{}} ret
     * @return {{}}
     * @private
     */
    _createWhere(ret) {
        if (this._filters.length > 0) {
            ret['where'] = {};
            this._filters.forEach(filter => {
                ret['where'][filter.field] = filter.expression;
            });
        }
        return ret;
    }

    /**
     *
     * @param {[]} fields
     * @return {DBModelQueryInterface}
     */
    setFields(fields) {
        this._fieldsList = fields;
        return this;
    }

    /**
     *
     * @param {string} field
     * @param {string} direction
     * @return {DBModelQueryInterface}
     */
    addOrder(field, direction) {
        this._orders.push([field, direction]);
        return undefined;
    }

    /**
     *
     * @param {number} offset
     * @param {number} limit
     * @return {DBModelQueryInterface}
     */
    setLimit(offset, limit) {
        this._limit.limit = limit;
        this._limit.offset = offset;
        return this;
    }

    /**
     *
     * @param {string} field
     * @param {*} expression
     */
    addFilter(field, expression) {
        this._filters.push({field: field, expression: expression});
        return this;
    }

    /**
     *
     * @param {DBModelRelationInterface} relation
     * @return {DBModelQueryInterface}
     */
    addRelation(relation) {
        this._relations.push(relation);
        return this;
    }
}

module.exports = SequelizeQueryAdapter;
