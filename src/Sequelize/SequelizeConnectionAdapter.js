'use strict';
const DBConnectionInterface = require('w-node-db-connection');
const SequelizeModelAdapter = require('./SequelizeModelAdapter');
/**
 * @class DBConnectionSequelizeAdapter
 * @type {DBConnectionInterface}
 * @extends {DBConnectionInterface}
 */
module.exports = class DBConnectionSequelizeAdapter extends DBConnectionInterface {

    /**
     *
     * @param {*} Sequelize
     * @param {string} path
     * @param {{}} options
     */
    constructor(Sequelize, path, options = {}) {
        super();
        /**
         *
         * @type {*}
         * @private
         */
        this._SequelizeClass = Sequelize;
        /**
         * @private
         */
        this._sequelize = new this._SequelizeClass(path);
        /**
         *
         * @type {{}}
         * @private
         */
        this._options = {};
        this._options = this._extendedOptions(options);
    }

    /**
     * @param {{}| null} options
     * @return {*}
     */
    // eslint-disable-next-line no-unused-vars
    sync(options = null) {
        return this._sequelize.sync(this._extendedOptions(options));
    }

    /**
     *
     * @param {DBModelDefinitionInterface} modelDefinition
     * @return {DBModelInterface}
     */
    define(modelDefinition) {
        let sequelizeModel = this._applyRelation(
            modelDefinition,
            this._initSequelizeModel(modelDefinition)
        );
        return new SequelizeModelAdapter(sequelizeModel, modelDefinition);
    }

    /**
     *
     * @param {DBModelDefinitionInterface} modelDefinition
     * @return {Model}
     * @private
     */
    _initSequelizeModel(modelDefinition) {
        const sequelize = this._sequelize;
        const sequelizeModel = class extends this._SequelizeClass.Model {
        };

        sequelizeModel.init(
            this._getModelAttributes(modelDefinition),
            {
                sequelize,
                modelName: modelDefinition.getModelName(),
                indexes: this._getModelIndexes(modelDefinition),
            }
        );

        return sequelizeModel;
    }

    /**
     *
     * @param  {DBModelDefinitionInterface} modelDefinition
     * @param sequelizeModel
     * @return {Model}
     * @private
     */
    _applyRelation(modelDefinition, sequelizeModel) {
        const relation = modelDefinition.getRelation();
        if (relation) {
            const relationMethod = this._createRelationMethod(relation);
            sequelizeModel[relationMethod](
                this._initSequelizeModel(relation.getDefinition()),
                {as: relation.getRelationAlias()}
            );
        }
        return sequelizeModel;
    }

    /**
     *
     * @param {DBModelDefinitionInterface} model
     * @return {Array}
     * @private
     */
    _getModelIndexes(model) {
        return model
            .getIndexes()
            .map(index => ({
                unique: index.isUnique(),
                fields: index.getFieldsNames()
            }));
    }

    /**
     *
     * @param {DBModelDefinitionInterface} model
     * @return {{}}
     * @private
     */
    _getModelAttributes(model) {
        const ret = {};
        model.getColumns()
            .forEach(column => {
                ret[column.getName()] = this._createColumnDefinition(column);
            });
        return ret;
    }

    /**
     *
     * @param {DBModelColumnDefinitionInterface} column
     * @return {{allowNull: *, type: *}}
     * @private
     */
    _createColumnDefinition(column) {
        const colDefinition = {
            type: this._createType(
                column.getType(),
                column.getSize(),
                column.getDecimal(),
                column.getTypeOptions()
            ),
            allowNull: column.isAllowNull()
        };

        if (column.isAllowNull() === true) {
            colDefinition['defaultValue'] = column.getDefaultValue();
        }

        return colDefinition;
    }

    /**
     *
     * @param {string} type
     * @param {int} size
     * @param {int|null} decimal
     * @private
     */
    _createType(type, size, decimal = null, typeOption = null) {
        switch (type) {
            case 'string':
                // eslint-disable-next-line new-cap
                return this._SequelizeClass.STRING(size);
            case 'int':
                // eslint-disable-next-line new-cap
                return this._SequelizeClass.BIGINT(size);
            case 'date':
                // eslint-disable-next-line new-cap
                return this._SequelizeClass.DATEONLY;
            case 'float':
                // eslint-disable-next-line new-cap
                return this._SequelizeClass.FLOAT(size, decimal);
            case 'enum':
                // eslint-disable-next-line new-cap
                return this._SequelizeClass.ENUM(...typeOption);
            case 'text':
                // eslint-disable-next-line new-cap
                return this._SequelizeClass.TEXT;
        }
    }

    _extendedOptions(options) {
        return Object.assign(this._options, options);
    }

    /**
     *
     * @param {DBModelRelationInterface} relation
     * @return {string}
     * @private
     */
    _createRelationMethod(relation) {
        switch (relation.getRelationType()) {
            case 'hasMany':
                return 'hasMany';
        }
        return '';
    }
};
