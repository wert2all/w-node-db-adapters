'use strict';

/**
 * The entry point.
 *
 * @module DBAdapters
 */
module.exports = {};
module.exports.model = require('./src/Sequelize/SequelizeModelAdapter');
module.exports.connection = require('./src/Sequelize/SequelizeConnectionAdapter');
module.exports.query = require('./src/Sequelize/SequelizeQueryAdapter');

